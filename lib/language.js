let currentLanguageValue = null;

const isString = value => typeof (value) === 'string';
const includes = (array, element) => array.indexOf(element) !== -1;

class Language {
  static currentLanguage(_currentLanguage = currentLanguageValue) {
    if (this.isValid(_currentLanguage)) {
      return _currentLanguage;
    }
    currentLanguageValue = Language.defaultLanguage();


    return currentLanguageValue;
  }

  static defaultLanguage() {
    if (this.isValid(global.LANG)) {
      return global.LANG.toLowerCase();
    }
    return this.english();
  }

  static supportedLanguages() {
    return [
      this.russian(),
      this.english(),
      this.hindi(),
      this.polish(),
      this.german(),
      this.ukrainian(),
      this.thai(),
      this.punjabi(),
      this.malayalam(),
      this.gujarati(),
      this.kannada(),
      this.bengali(),
      this.telugu(),
      this.marathi(),
      this.tamil(),
    ];
  }

  static set(language) {
    if (this.isValid(language)) {
      currentLanguageValue = language.toLowerCase();
    }
  }

  static russian() {
    return 'ru';
  }

  static isCurrent(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.currentLanguage());
  }

  static isRussian(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.russian());
  }

  static english() {
    return 'en';
  }

  static isEnglish(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.english());
  }

  static ukrainian() {
    return 'uk';
  }

  static isUkrainian(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.ukrainian());
  }

  static hindi() {
    return 'hi';
  }

  static isHindi(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.hindi());
  }

  static german() {
    return 'de';
  }

  static isGerman(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.german());
  }

  static polish() {
    return 'pl';
  }

  static isPolish(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.polish());
  }

  static thai() {
    return 'th';
  }

  static isThai(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.thai());
  }

  static punjabi() {
    return 'pa';
  }

  static isPunjabi(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.punjabi());
  }

  static malayalam() {
    return 'ml';
  }

  static isMalayalam(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.malayalam());
  }

  static gujarati() {
    return 'gu';
  }

  static isGujarati(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.gujarati());
  }

  static kannada() {
    return 'kn';
  }

  static isKannada(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.kannada());
  }

  static bengali() {
    return 'bn';
  }

  static isBengali(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.bengali());
  }

  static telugu() {
    return 'te';
  }

  static isTelugu(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.telugu());
  }

  static marathi() {
    return 'mr';
  }

  static isMarathi(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.marathi());
  }

  static tamil() {
    return 'ta';
  }

  static isTamil(language) {
    return !!(this.isValid(language) && language.toLowerCase() === this.tamil());
  }

  static isValid(language) {
    return !!(language && isString(language) &&
      includes(Language.supportedLanguages(), language.toLowerCase()));
  }
}

module.exports = Language;
