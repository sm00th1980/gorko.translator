const Language = require('./language');

const FORMS = {
  RU: {
    ONE: 0,
    FEW: 1,
    MANY: 2,
  },
  EN: {
    ONE: 0,
    OTHER: 1,
  },
  HI: {
    ONE: 0,
    OTHER: 1,
  },
  UK: {
    ONE: 0,
    FEW: 1,
    MANY: 2,
    OTHER: 3,
  },
  DE: {
    ONE: 0,
    OTHER: 1,
  },
  PL: {
    ONE: 0,
    FEW: 1,
    MANY: 2,
    OTHER: 3,
  },
  TH: {
    OTHER: 0,
  },
  PA: {
    ONE: 0,
    OTHER: 1,
  },
  ML: {
    ONE: 0,
    OTHER: 1,
  },
  GU: {
    ONE: 0,
    OTHER: 1,
  },
  KN: {
    ONE: 0,
    OTHER: 1,
  },
  BN: {
    ONE: 0,
    OTHER: 1,
  },
  TE: {
    ONE: 0,
    OTHER: 1,
  },
  MR: {
    ONE: 0,
    OTHER: 1,
  },
  TA: {
    ONE: 0,
    OTHER: 1,
  },
};

const isInt = n => Number(n) === n && n % 1 === 0;

class Plural {
  static form(lang, n) {
    if (Language.isRussian(lang)) {
      if (isInt(n)) {
        if (n % 10 === 1 && n % 100 !== 11) {
          return FORMS.RU.ONE;
        }

        if (n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)) {
          return FORMS.RU.FEW;
        }

        return FORMS.RU.MANY;
      }

      return FORMS.RU.FEW;
    }

    if (Language.isEnglish(lang)) {
      if (isInt(n) && n === 1) {
        return FORMS.EN.ONE;
      }

      return FORMS.EN.OTHER;
    }

    if (Language.isHindi(lang)) {
      if (isInt(n) && (n === 0 || n === 1)) {
        return FORMS.HI.ONE;
      }

      return FORMS.HI.OTHER;
    }

    if (Language.isUkrainian(lang)) {
      if (isInt(n)) {
        if (n % 10 === 1 && n % 100 !== 11) {
          return 0;
        }

        if (n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)) {
          return 1;
        }

        return 2;
      }

      return FORMS.UK.OTHER;
    }

    if (Language.isGerman(lang)) {
      if (isInt(n) && n === 1) {
        return FORMS.DE.ONE;
      }

      return FORMS.DE.OTHER;
    }

    if (Language.isPolish(lang)) {
      if (isInt(n)) {
        if (n === 1) {
          return 0;
        }

        if (n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)) {
          return 1;
        }

        return 2;
      }

      return FORMS.PL.OTHER;
    }

    if (Language.isThai(lang)) {
      return FORMS.TH.OTHER;
    }

    if (Language.isPunjabi(lang)) {
      if (isInt(n) && (n === 0 || n === 1)) {
        return FORMS.PA.ONE;
      }

      return FORMS.PA.OTHER;
    }

    if (Language.isGujarati(lang)) {
      if (isInt(n) && (n === 0 || n === 1)) {
        return FORMS.GU.ONE;
      }

      return FORMS.GU.OTHER;
    }

    if (Language.isKannada(lang)) {
      if (isInt(n) && (n === 0 || n === 1)) {
        return FORMS.KN.ONE;
      }

      return FORMS.KN.OTHER;
    }

    if (Language.isBengali(lang)) {
      if (isInt(n) && (n === 0 || n === 1)) {
        return FORMS.BN.ONE;
      }

      return FORMS.BN.OTHER;
    }

    if (Language.isTelugu(lang)) {
      if (isInt(n) && n === 1) {
        return FORMS.TE.ONE;
      }

      return FORMS.TE.OTHER;
    }

    if (Language.isMalayalam(lang)) {
      if (isInt(n) && n === 1) {
        return FORMS.ML.ONE;
      }

      return FORMS.ML.OTHER;
    }

    if (Language.isMarathi(lang)) {
      if (isInt(n) && (n === 0 || n === 1)) {
        return FORMS.MR.ONE;
      }

      return FORMS.MR.OTHER;
    }

    if (Language.isTamil(lang)) {
      if (isInt(n) && n === 1) {
        return FORMS.TA.ONE;
      }

      return FORMS.TA.OTHER;
    }

    return undefined;
  }

  static isInt(n) {
    return Number(n) === n && n % 1 === 0;
  }
}

module.exports = { form: Plural.form, FORMS };
