/* eslint "no-eval": "off" */

const React = require('react');
const ReactDOMServer = require('react-dom/server');

const Plural = require('./plural');
const Language = require('./language');

const isUndefined = value => typeof (value) === 'undefined';

const Gorko = {
  translate: {
    dictionary: {},
    config: { language: '' },
  },
};

/**
 * Process translations
 *
 * @param message the message to be translated
 * @param params array of parameters (number, placeholders)
 * @param dictionary instance of dictionary
 * @return translated string
 */
Gorko.translate.process = function process(message, params_, dictionary) {
  let params = params_;
  // try to translate string
  let translation = dictionary && !isUndefined(dictionary[message])
    ? dictionary[message]
    : message;

  if (isUndefined(params)) {
    params = 0;
  }

  // declare numeric param
  let num = 0;

  // extract number from params
  if (Number(params) || params === 0) {
    params = { n: params }; // param is numeric, convert to object key for convenience
  }

  if (Number(params.n) || params.n === 0) {
    num = params.n;
  }

  Object.keys(params).forEach((key) => {
    if (React.isValidElement(params[key])) {
      params[key] = ReactDOMServer.renderToStaticMarkup(params[key]);
    }
  });

  // split translation into pieces
  const chunks = translation.split('|');

  if (translation.indexOf('<###>') !== -1) { // translation contains expression
    for (let i = 0; i < chunks.length; i += 1) {
      const pieces = chunks[i].split('<###>'); // split each chunk in two parts (0: expression, 1: message)
      let ex = pieces[0];
      const msg = pieces[1];

      if (pieces.length === 2) {
        // handle number shortcut (0 instead of n==0)
        if (ex % 1 === 0) {
          ex = `n==${ex}`;
        }

        // create expression to be evaluated (e.g. n>3)
        const evalExpr = ex.split('n').join(num);

        // if expression matches, set translation to current chunk
        if (eval(evalExpr)) {
          translation = msg;
          break;
        }
      }
    }
  } else if (chunks.length > 1) {
    // if translation doesn't contain # but does contain |, treat it as simple choice format

    const index = Plural.form(Language.currentLanguage(), num);
    translation = chunks[index];
  }

  // replace placeholder/replacements
  if (typeof params === 'object') {
    Object.keys(params).forEach((key) => {
      translation = translation.split(`{${key}}`).join(params[key]);
    });
  }

  return translation;
};

/**
 * Wrapper for port Gorko::t
 *
 * @param message the message to be translated
 * @param params array of parameters (number, placeholders)
 * @return translated message
 */

class Translator {
  static t(text, data) {
    return Gorko.translate.process(text, data, false);
  }
}

module.exports = Translator;
