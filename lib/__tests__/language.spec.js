import _ from 'lodash';
import Language from '../language';

describe('language service', () => {
  describe('language service', () => {
    it('should has correct supported languages', () => {
      const sortBy = _.partial(_.sortBy, _, lang => lang);

      expect(sortBy(Language.supportedLanguages())).toEqual(sortBy(['bn', 'de', 'en', 'gu', 'hi', 'kn', 'ml', 'mr', 'pa', 'pl', 'ru', 'ta', 'te', 'th', 'uk']));
    });

    it('should has correct set language from supported list', () => {
      const language = _.sample(Language.supportedLanguages());

      Language.set(language);
      expect(Language.currentLanguage()).toBe(language);
    });

    it('should not update current language if input lang is undefined', () => {
      const currentLanguage = Language.currentLanguage();
      expect(_.includes(Language.supportedLanguages(), currentLanguage)).toBe(true);

      Language.set(undefined);
      expect(Language.currentLanguage()).toBe(currentLanguage);
    });

    it('should not update current language if input lang is null', () => {
      const currentLanguage = Language.currentLanguage();
      expect(_.includes(Language.supportedLanguages(), currentLanguage)).toBe(true);

      Language.set(null);
      expect(Language.currentLanguage()).toBe(currentLanguage);
    });

    it('should correct set language from supported list in upper case', () => {
      const language = _.sample(Language.supportedLanguages());

      Language.set(language.toUpperCase());
      expect(Language.currentLanguage()).toBe(language);
    });
  });

  describe('language service default-language', () => {
    it('should has correct default language if gloabl LANG is undefined', () => {
      global.LANG = undefined;
      expect(global.LANG).toBeUndefined();
      expect(Language.defaultLanguage()).toBe('en');
    });

    it('should has correct default language if global LANG is set to supported language', () => {
      const lang = _.sample(Language.supportedLanguages());
      global.LANG = lang;

      expect(global.LANG).toBe(lang);

      expect(Language.defaultLanguage()).toBe(lang);
    });

    it('should has correct default language if global LANG is set to not supported language', () => {
      const lang = 'chinise';
      global.LANG = lang;

      expect(global.LANG).toBe(lang);

      expect(Language.defaultLanguage()).toBe('en');
    });

    afterAll(() => {
      global.LANG = undefined;
    });
  });

  describe('language service', () => {
    it('should has correct languages', () => {
      expect(Language.russian()).toBe('ru');
      expect(Language.english()).toBe('en');
      expect(Language.ukrainian()).toBe('uk');
      expect(Language.hindi()).toBe('hi');
      expect(Language.german()).toBe('de');
      expect(Language.polish()).toBe('pl');
    });

    it('should has correct check isRussian', () => {
      expect(Language.isRussian('ru')).toBe(true);
      expect(Language.isRussian('Ru')).toBe(true);
      expect(Language.isRussian('rU')).toBe(true);
      expect(Language.isRussian('RU')).toBe(true);

      expect(Language.isRussian(Language.english())).toBe(false);
      expect(Language.isRussian(Language.ukrainian())).toBe(false);
      expect(Language.isRussian(Language.hindi())).toBe(false);
      expect(Language.isRussian(Language.german())).toBe(false);
      expect(Language.isRussian(Language.polish())).toBe(false);
    });

    it('should has correct check isEnglish', () => {
      expect(Language.isEnglish('en')).toBe(true);
      expect(Language.isEnglish('En')).toBe(true);
      expect(Language.isEnglish('eN')).toBe(true);
      expect(Language.isEnglish('EN')).toBe(true);

      expect(Language.isEnglish(Language.russian())).toBe(false);
      expect(Language.isEnglish(Language.ukrainian())).toBe(false);
      expect(Language.isEnglish(Language.hindi())).toBe(false);
      expect(Language.isEnglish(Language.german())).toBe(false);
      expect(Language.isEnglish(Language.polish())).toBe(false);
    });

    it('should has correct check isUkrainian', () => {
      expect(Language.isUkrainian('uk')).toBe(true);
      expect(Language.isUkrainian('Uk')).toBe(true);
      expect(Language.isUkrainian('uK')).toBe(true);
      expect(Language.isUkrainian('UK')).toBe(true);

      expect(Language.isUkrainian(Language.russian())).toBe(false);
      expect(Language.isUkrainian(Language.english())).toBe(false);
      expect(Language.isUkrainian(Language.hindi())).toBe(false);
      expect(Language.isUkrainian(Language.german())).toBe(false);
      expect(Language.isUkrainian(Language.polish())).toBe(false);
    });

    it('should has correct check isHindi', () => {
      expect(Language.isHindi('hi')).toBe(true);
      expect(Language.isHindi('Hi')).toBe(true);
      expect(Language.isHindi('hI')).toBe(true);
      expect(Language.isHindi('HI')).toBe(true);

      expect(Language.isHindi(Language.russian())).toBe(false);
      expect(Language.isHindi(Language.english())).toBe(false);
      expect(Language.isHindi(Language.ukrainian())).toBe(false);
      expect(Language.isHindi(Language.german())).toBe(false);
      expect(Language.isHindi(Language.polish())).toBe(false);
    });

    it('should has correct check isGerman', () => {
      expect(Language.isGerman('de')).toBe(true);
      expect(Language.isGerman('De')).toBe(true);
      expect(Language.isGerman('dE')).toBe(true);
      expect(Language.isGerman('DE')).toBe(true);

      expect(Language.isGerman(Language.russian())).toBe(false);
      expect(Language.isGerman(Language.english())).toBe(false);
      expect(Language.isGerman(Language.ukrainian())).toBe(false);
      expect(Language.isGerman(Language.hindi())).toBe(false);
      expect(Language.isGerman(Language.polish())).toBe(false);
    });

    it('should has correct check isPolish', () => {
      expect(Language.isPolish('pl')).toBe(true);
      expect(Language.isPolish('Pl')).toBe(true);
      expect(Language.isPolish('pL')).toBe(true);
      expect(Language.isPolish('PL')).toBe(true);

      expect(Language.isPolish(Language.russian())).toBe(false);
      expect(Language.isPolish(Language.english())).toBe(false);
      expect(Language.isPolish(Language.ukrainian())).toBe(false);
      expect(Language.isPolish(Language.hindi())).toBe(false);
      expect(Language.isPolish(Language.german())).toBe(false);
    });
  });

  describe('language service', () => {
    it('should check isValid', () => {
      expect(Language.isValid(Language.russian())).toBe(true);
      expect(Language.isValid(Language.english())).toBe(true);
      expect(Language.isValid(Language.ukrainian())).toBe(true);
      expect(Language.isValid(Language.hindi())).toBe(true);
      expect(Language.isValid(Language.german())).toBe(true);
      expect(Language.isValid(Language.polish())).toBe(true);
    });

    it('should check isValid', () => {
      expect(Language.isValid(undefined)).toBe(false);
      expect(Language.isValid(null)).toBe(false);
      expect(Language.isValid(1)).toBe(false);
      expect(Language.isValid(0)).toBe(false);
      expect(Language.isValid(78.9)).toBe(false);
      expect(Language.isValid('cfcfd')).toBe(false);
      expect(Language.isValid(NaN)).toBe(false);
      expect(Language.isValid(Infinity)).toBe(false);
      expect(Language.isValid(-Infinity)).toBe(false);
    });
  });
});
