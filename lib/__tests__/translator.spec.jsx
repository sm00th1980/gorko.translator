import React from 'react';
import PropTypes from 'prop-types';
import Translator from '../translator';
import Language from '../language';

describe('translate english', () => {
  beforeEach(() => {
    Language.set(Language.english());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('Apple')).toEqual('Apple');
    expect(Translator.t('Apples')).toEqual('Apples');
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('The organization&#39;s.')).toEqual('The organization&#39;s.');
  });

  it('should translate plural form correctly', () => {
    expect(Translator.t('Apple|Apples', 0)).toEqual('Apples');
    expect(Translator.t('Apple|Apples', 1)).toEqual('Apple');
    expect(Translator.t('Apple|Apples', 2)).toEqual('Apples');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} Apple|{n} Apples', 0)).toEqual('0 Apples');
    expect(Translator.t('{n} Apple|{n} Apples', 1)).toEqual('1 Apple');
    expect(Translator.t('{n} Apple|{n} Apples', 2)).toEqual('2 Apples');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>No comments, be the first!|1<###>One comment|n>1<###>{n} comments', 0)).toEqual('No comments, be the first!');
    expect(Translator.t('0<###>No comments, be the first!|1<###>One comment|n>1<###>{n} comments', 1)).toEqual('One comment');
    expect(Translator.t('0<###>No comments, be the first!|1<###>One comment|n>1<###>{n} comments', 2)).toEqual('2 comments');
  });

  it('should translate plural form with expressions and placeholders correctly', () => {
    expect(Translator.t('0<###>{name} has no mail|1<###>{name} has one mail|n>1<###>{name} has {n} mails', {
      n: 0,
      name: 'Pete',
    })).toEqual('Pete has no mail');
    expect(Translator.t('0<###>{name} has no mail|1<###>{name} has one mail|n>1<###>{name} has {n} mails', {
      n: 1,
      name: 'Pete',
    })).toEqual('Pete has one mail');
    expect(Translator.t('0<###>{name} has no mail|1<###>{name} has one mail|n>1<###>{name} has {n} mails', {
      n: 2,
      name: 'Pete',
    })).toEqual('Pete has 2 mails');
  });
});

describe('translate russian', () => {
  beforeEach(() => {
    Language.set(Language.russian());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('Яблоко')).toEqual('Яблоко');
    expect(Translator.t('Яблоки')).toEqual('Яблоки');
  });

  it('should translate plural form correctly', () => {
    expect(Translator.t('Яблоко|Яблока|Яблок', 0)).toEqual('Яблок');
    expect(Translator.t('Яблоко|Яблока|Яблок', 1)).toEqual('Яблоко');
    expect(Translator.t('Яблоко|Яблока|Яблок', 2)).toEqual('Яблока');
    expect(Translator.t('Яблоко|Яблока|Яблок', 3)).toEqual('Яблока');
    expect(Translator.t('Яблоко|Яблока|Яблок', 5)).toEqual('Яблок');
    expect(Translator.t('Яблоко|Яблока|Яблок', 1.2)).toEqual('Яблока');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} Яблоко|{n} Яблока|{n} Яблок', 0)).toEqual('0 Яблок');
    expect(Translator.t('{n} Яблоко|{n} Яблока|{n} Яблок', 1)).toEqual('1 Яблоко');
    expect(Translator.t('{n} Яблоко|{n} Яблока|{n} Яблок', 2)).toEqual('2 Яблока');
    expect(Translator.t('{n} Яблоко|{n} Яблока|{n} Яблок', 3)).toEqual('3 Яблока');
    expect(Translator.t('{n} Яблоко|{n} Яблока|{n} Яблок', 5)).toEqual('5 Яблок');
    expect(Translator.t('{n} Яблоко|{n} Яблока|{n} Яблок', 1.2)).toEqual('1.2 Яблока');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>Нет комментариев|1<###>Один комментарий|n<=4<###>{n} комментария|n>4<###>{n} комментариев', 0)).toEqual('Нет комментариев');
    expect(Translator.t('0<###>Нет комментариев|1<###>Один комментарий|n<=4<###>{n} комментария|n>4<###>{n} комментариев', 1)).toEqual('Один комментарий');
    expect(Translator.t('0<###>Нет комментариев|1<###>Один комментарий|n<=4<###>{n} комментария|n>4<###>{n} комментариев', 2)).toEqual('2 комментария');
    expect(Translator.t('0<###>Нет комментариев|1<###>Один комментарий|n<=4<###>{n} комментария|n>4<###>{n} комментариев', 3)).toEqual('3 комментария');
    expect(Translator.t('0<###>Нет комментариев|1<###>Один комментарий|n<=4<###>{n} комментария|n>4<###>{n} комментариев', 4)).toEqual('4 комментария');
    expect(Translator.t('0<###>Нет комментариев|1<###>Один комментарий|n<=4<###>{n} комментария|n>4<###>{n} комментариев', 5)).toEqual('5 комментариев');
  });

  it('should translate plural form with expressions and placeholders correctly', () => {
    expect(Translator.t('0<###>{name} не имеет сообщений|1<###>{name} имеет {n} сообщение|n<=4<###>{name} имеет {n} сообщения|n>4<###>{name} имеет {n} сообщений', {
      n: 0,
      name: 'Петя',
    })).toEqual('Петя не имеет сообщений');
    expect(Translator.t('0<###>{name} не имеет сообщений|1<###>{name} имеет {n} сообщение|n<=4<###>{name} имеет {n} сообщения|n>4<###>{name} имеет {n} сообщений', {
      n: 1,
      name: 'Петя',
    })).toEqual('Петя имеет 1 сообщение');
    expect(Translator.t('0<###>{name} не имеет сообщений|1<###>{name} имеет {n} сообщение|n<=4<###>{name} имеет {n} сообщения|n>4<###>{name} имеет {n} сообщений', {
      n: 2,
      name: 'Петя',
    })).toEqual('Петя имеет 2 сообщения');
    expect(Translator.t('0<###>{name} не имеет сообщений|1<###>{name} имеет {n} сообщение|n<=4<###>{name} имеет {n} сообщения|n>4<###>{name} имеет {n} сообщений', {
      n: 5,
      name: 'Петя',
    })).toEqual('Петя имеет 5 сообщений');
  });


  it('more samples', () => {
    expect(Translator.t('У Вас загружен {n} фотоотчет|У Вас загружено {n} фотоотчета|У Вас загружено {n} фотоотчетов', 0)).toEqual('У Вас загружено 0 фотоотчетов');
    expect(Translator.t('У Вас загружен {n} фотоотчет|У Вас загружено {n} фотоотчета|У Вас загружено {n} фотоотчетов', 1)).toEqual('У Вас загружен 1 фотоотчет');
    expect(Translator.t('У Вас загружен {n} фотоотчет|У Вас загружено {n} фотоотчета|У Вас загружено {n} фотоотчетов', 2)).toEqual('У Вас загружено 2 фотоотчета');
    expect(Translator.t('У Вас загружен {n} фотоотчет|У Вас загружено {n} фотоотчета|У Вас загружено {n} фотоотчетов', 3)).toEqual('У Вас загружено 3 фотоотчета');
    expect(Translator.t('У Вас загружен {n} фотоотчет|У Вас загружено {n} фотоотчета|У Вас загружено {n} фотоотчетов', 5)).toEqual('У Вас загружено 5 фотоотчетов');
  });

  it('more samples', () => {
    expect(Translator.t('Активен {n} день|Активен {n} дня|Активен {n} дней', 0)).toEqual('Активен 0 дней');
    expect(Translator.t('Активен {n} день|Активен {n} дня|Активен {n} дней', 1)).toEqual('Активен 1 день');
    expect(Translator.t('Активен {n} день|Активен {n} дня|Активен {n} дней', 2)).toEqual('Активен 2 дня');
    expect(Translator.t('Активен {n} день|Активен {n} дня|Активен {n} дней', 3)).toEqual('Активен 3 дня');
    expect(Translator.t('Активен {n} день|Активен {n} дня|Активен {n} дней', 5)).toEqual('Активен 5 дней');
    expect(Translator.t('Активен {n} день|Активен {n} дня|Активен {n} дней', 570)).toEqual('Активен 570 дней');
  });

  it('more samples', () => {
    expect(Translator.t('{n} комментарий перенесён в тему {topic}|{n} комментария перенесены в тему {topic}|{n} комментариев перенесено в тему {topic}', {
      n: 0,
      topic: 'Тема',
    })).toEqual('0 комментариев перенесено в тему Тема');
    expect(Translator.t('{n} комментарий перенесён в тему {topic}|{n} комментария перенесены в тему {topic}|{n} комментариев перенесено в тему {topic}', {
      n: 1,
      topic: 'Тема',
    })).toEqual('1 комментарий перенесён в тему Тема');
    expect(Translator.t('{n} комментарий перенесён в тему {topic}|{n} комментария перенесены в тему {topic}|{n} комментариев перенесено в тему {topic}', {
      n: 2,
      topic: 'Тема',
    })).toEqual('2 комментария перенесены в тему Тема');
    expect(Translator.t('{n} комментарий перенесён в тему {topic}|{n} комментария перенесены в тему {topic}|{n} комментариев перенесено в тему {topic}', {
      n: 5,
      topic: 'Тема',
    })).toEqual('5 комментариев перенесено в тему Тема');
  });

  it('more samples with html tags inside', () => {
    expect(Translator.t('Фото на <b>{place} месте</b> с {points}', {
      place: '<span id="contest_photo_place">123</span>',
      points: '<span id="contest_photo_points">456</span>',
    })).toEqual('Фото на <b><span id="contest_photo_place">123</span> месте</b> с <span id="contest_photo_points">456</span>');
  });

  it('more samples with html link tag inside', () => {
    expect(Translator.t('автор: {link}', {
      link: '<a href="http://www.yandex.ru">Деяров Руслан</a>',
    })).toEqual('автор: <a href="http://www.yandex.ru">Деяров Руслан</a>');
  });

  it('more samples with double quotes', () => {
    expect(Translator.t('{n} комментарий перенесён в тему "{topic}"|{n} комментария перенесены в тему "{topic}"|{n} комментариев перенесено в тему "{topic}"', {
      n: 0,
      topic: 'Тема',
    })).toEqual('0 комментариев перенесено в тему "Тема"');
    expect(Translator.t('{n} комментарий перенесён в тему "{topic}"|{n} комментария перенесены в тему "{topic}"|{n} комментариев перенесено в тему "{topic}"', {
      n: 1,
      topic: 'Тема',
    })).toEqual('1 комментарий перенесён в тему "Тема"');
    expect(Translator.t('{n} комментарий перенесён в тему "{topic}"|{n} комментария перенесены в тему "{topic}"|{n} комментариев перенесено в тему "{topic}"', {
      n: 2,
      topic: 'Тема',
    })).toEqual('2 комментария перенесены в тему "Тема"');
    expect(Translator.t('{n} комментарий перенесён в тему "{topic}"|{n} комментария перенесены в тему "{topic}"|{n} комментариев перенесено в тему "{topic}"', {
      n: 5,
      topic: 'Тема',
    })).toEqual('5 комментариев перенесено в тему "Тема"');
  });

  it('more samples with single quotes', () => {
    expect(Translator.t("{n} комментарий перенесён в тему '{topic}'|{n} комментария перенесены в тему '{topic}'|{n} комментариев перенесено в тему '{topic}'", {
      n: 0,
      topic: 'Тема',
    })).toEqual("0 комментариев перенесено в тему 'Тема'");
    expect(Translator.t("{n} комментарий перенесён в тему '{topic}'|{n} комментария перенесены в тему '{topic}'|{n} комментариев перенесено в тему '{topic}'", {
      n: 1,
      topic: 'Тема',
    })).toEqual("1 комментарий перенесён в тему 'Тема'");
    expect(Translator.t("{n} комментарий перенесён в тему '{topic}'|{n} комментария перенесены в тему '{topic}'|{n} комментариев перенесено в тему '{topic}'", {
      n: 2,
      topic: 'Тема',
    })).toEqual("2 комментария перенесены в тему 'Тема'");
    expect(Translator.t("{n} комментарий перенесён в тему '{topic}'|{n} комментария перенесены в тему '{topic}'|{n} комментариев перенесено в тему '{topic}'", {
      n: 5,
      topic: 'Тема',
    })).toEqual("5 комментариев перенесено в тему 'Тема'");
  });
});

describe('translate hindi', () => {
  beforeEach(() => {
    Language.set(Language.hindi());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('सेब')).toEqual('सेब');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} सेब|सेब की {n}', 0)).toEqual('0 सेब');
    expect(Translator.t('{n} सेब|सेब की {n}', 1)).toEqual('1 सेब');
    expect(Translator.t('{n} सेब|सेब की {n}', 2)).toEqual('सेब की 2');
    expect(Translator.t('{n} सेब|सेब की {n}', 3)).toEqual('सेब की 3');
    expect(Translator.t('{n} सेब|सेब की {n}', 1.2)).toEqual('सेब की 1.2');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>कोई टिप्पणी नहीं|1<###>एक टिप्पणी|n>1<###>{n} टिप्पणियो', 0)).toEqual('कोई टिप्पणी नहीं');
    expect(Translator.t('0<###>कोई टिप्पणी नहीं|1<###>एक टिप्पणी|n>1<###>{n} टिप्पणियो', 1)).toEqual('एक टिप्पणी');
    expect(Translator.t('0<###>कोई टिप्पणी नहीं|1<###>एक टिप्पणी|n>1<###>{n} टिप्पणियो', 2)).toEqual('2 टिप्पणियो');
    expect(Translator.t('0<###>कोई टिप्पणी नहीं|1<###>एक टिप्पणी|n>1<###>{n} टिप्पणियो', 1.2)).toEqual('1.2 टिप्पणियो');
  });
});

describe('translate ukrainian', () => {
  beforeEach(() => {
    Language.set(Language.ukrainian());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('яблуко')).toEqual('яблуко');
    expect(Translator.t('яблука')).toEqual('яблука');
  });

  it('should translate plural form correctly', () => {
    expect(Translator.t('яблуко|яблука|яблук|яблук', 0)).toEqual('яблук');
    expect(Translator.t('яблуко|яблука|яблук|яблук', 1)).toEqual('яблуко');
    expect(Translator.t('яблуко|яблука|яблук|яблук', 2)).toEqual('яблука');
    expect(Translator.t('яблуко|яблука|яблук|яблук', 3)).toEqual('яблука');
    expect(Translator.t('яблуко|яблука|яблук|яблук', 5)).toEqual('яблук');
    expect(Translator.t('яблуко|яблука|яблук|яблук', 1.2)).toEqual('яблук');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} яблуко|{n} яблука|{n} яблук|{n} яблук', 0)).toEqual('0 яблук');
    expect(Translator.t('{n} яблуко|{n} яблука|{n} яблук|{n} яблук', 1)).toEqual('1 яблуко');
    expect(Translator.t('{n} яблуко|{n} яблука|{n} яблук|{n} яблук', 2)).toEqual('2 яблука');
    expect(Translator.t('{n} яблуко|{n} яблука|{n} яблук|{n} яблук', 3)).toEqual('3 яблука');
    expect(Translator.t('{n} яблуко|{n} яблука|{n} яблук|{n} яблук', 5)).toEqual('5 яблук');
    expect(Translator.t('{n} яблуко|{n} яблука|{n} яблук|{n} яблук', 1.2)).toEqual('1.2 яблук');
  });
});

describe('translate polish', () => {
  beforeEach(() => {
    Language.set(Language.polish());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('jabłko')).toEqual('jabłko');
    expect(Translator.t('jabłka')).toEqual('jabłka');
  });

  it('should translate plural form correctly', () => {
    expect(Translator.t('jabłko|jabłka|jabłek|jabłek', 0)).toEqual('jabłek');
    expect(Translator.t('jabłko|jabłka|jabłek|jabłek', 1)).toEqual('jabłko');
    expect(Translator.t('jabłko|jabłka|jabłek|jabłek', 2)).toEqual('jabłka');
    expect(Translator.t('jabłko|jabłka|jabłek|jabłek', 3)).toEqual('jabłka');
    expect(Translator.t('jabłko|jabłka|jabłek|jabłek', 5)).toEqual('jabłek');
    expect(Translator.t('jabłko|jabłka|jabłek|jabłek', 1.2)).toEqual('jabłek');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} jabłko|{n} jabłka|{n} jabłek|{n} jabłek', 0)).toEqual('0 jabłek');
    expect(Translator.t('{n} jabłko|{n} jabłka|{n} jabłek|{n} jabłek', 1)).toEqual('1 jabłko');
    expect(Translator.t('{n} jabłko|{n} jabłka|{n} jabłek|{n} jabłek', 2)).toEqual('2 jabłka');
    expect(Translator.t('{n} jabłko|{n} jabłka|{n} jabłek|{n} jabłek', 3)).toEqual('3 jabłka');
    expect(Translator.t('{n} jabłko|{n} jabłka|{n} jabłek|{n} jabłek', 5)).toEqual('5 jabłek');
    expect(Translator.t('{n} jabłko|{n} jabłka|{n} jabłek|{n} jabłek', 1.2)).toEqual('1.2 jabłek');
  });
});

describe('translate german', () => {
  beforeEach(() => {
    Language.set(Language.german());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('Apfel')).toEqual('Apfel');
    expect(Translator.t('Äpfel')).toEqual('Äpfel');
  });

  it('should translate plural form correctly', () => {
    expect(Translator.t('Apfel|Äpfel', 0)).toEqual('Äpfel');
    expect(Translator.t('Apfel|Äpfel', 1)).toEqual('Apfel');
    expect(Translator.t('Apfel|Äpfel', 2)).toEqual('Äpfel');
    expect(Translator.t('Apfel|Äpfel', 5)).toEqual('Äpfel');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} Apfel|{n} Äpfel', 0)).toEqual('0 Äpfel');
    expect(Translator.t('{n} Apfel|{n} Äpfel', 1)).toEqual('1 Apfel');
    expect(Translator.t('{n} Apfel|{n} Äpfel', 2)).toEqual('2 Äpfel');
    expect(Translator.t('{n} Apfel|{n} Äpfel', 5)).toEqual('5 Äpfel');
    expect(Translator.t('{n} Apfel|{n} Äpfel', 1.2)).toEqual('1.2 Äpfel');
  });
});

describe('translate thai', () => {
  beforeEach(() => {
    Language.set(Language.thai());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('แอปเปิล')).toEqual('แอปเปิล');
    expect(Translator.t('แอปเปิ้ล')).toEqual('แอปเปิ้ล');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} แอปเปิ', 0)).toEqual('0 แอปเปิ');
    expect(Translator.t('{n} แอปเปิ', 1)).toEqual('1 แอปเปิ');
    expect(Translator.t('{n} แอปเปิ', 2)).toEqual('2 แอปเปิ');
    expect(Translator.t('{n} แอปเปิ', 3)).toEqual('3 แอปเปิ');
    expect(Translator.t('{n} แอปเปิ', 5)).toEqual('5 แอปเปิ');
    expect(Translator.t('{n} แอปเปิ', 1.2)).toEqual('1.2 แอปเปิ');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>{n} แอปเปิ|1<###>{n} แอปเปิ|n>1<###>{n} แอปเปิ', 0)).toEqual('0 แอปเปิ');
    expect(Translator.t('0<###>{n} แอปเปิ|1<###>{n} แอปเปิ|n>1<###>{n} แอปเปิ', 1)).toEqual('1 แอปเปิ');
    expect(Translator.t('0<###>{n} แอปเปิ|1<###>{n} แอปเปิ|n>1<###>{n} แอปเปิ', 2)).toEqual('2 แอปเปิ');
  });
});

describe('translate punjabi', () => {
  beforeEach(() => {
    Language.set(Language.punjabi());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('ਸੇਬ')).toEqual('ਸੇਬ');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} ਸੇਬ|{n} ਸੇਬ', 0)).toEqual('0 ਸੇਬ');
    expect(Translator.t('{n} ਸੇਬ|{n} ਸੇਬ', 1)).toEqual('1 ਸੇਬ');
    expect(Translator.t('{n} ਸੇਬ|{n} ਸੇਬ', 2)).toEqual('2 ਸੇਬ');
    expect(Translator.t('{n} ਸੇਬ|{n} ਸੇਬ', 3)).toEqual('3 ਸੇਬ');
    expect(Translator.t('{n} ਸੇਬ|{n} ਸੇਬ', 1.2)).toEqual('1.2 ਸੇਬ');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>ਟਿੱਪਣੀ|1<###>ਟਿੱਪਣੀ|n>1<###>{n} ਟਿੱਪਣੀ', 0)).toEqual('ਟਿੱਪਣੀ');
    expect(Translator.t('0<###>ਟਿੱਪਣੀ|1<###>ਟਿੱਪਣੀ|n>1<###>{n} ਟਿੱਪਣੀ', 1)).toEqual('ਟਿੱਪਣੀ');
    expect(Translator.t('0<###>ਟਿੱਪਣੀ|1<###>ਟਿੱਪਣੀ|n>1<###>{n} ਟਿੱਪਣੀ', 2)).toEqual('2 ਟਿੱਪਣੀ');
    expect(Translator.t('0<###>ਟਿੱਪਣੀ|1<###>ਟਿੱਪਣੀ|n>1<###>{n} ਟਿੱਪਣੀ', 1.2)).toEqual('1.2 ਟਿੱਪਣੀ');
  });
});

describe('translate malayalam', () => {
  beforeEach(() => {
    Language.set(Language.malayalam());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('വ്യക്തി')).toEqual('വ്യക്തി');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} വ്യക്തി|{n} വ്യക്തികൾ', 0)).toEqual('0 വ്യക്തികൾ');
    expect(Translator.t('{n} വ്യക്തി|{n} വ്യക്തികൾ', 1)).toEqual('1 വ്യക്തി');
    expect(Translator.t('{n} വ്യക്തി|{n} വ്യക്തികൾ', 2)).toEqual('2 വ്യക്തികൾ');
    expect(Translator.t('{n} വ്യക്തി|{n} വ്യക്തികൾ', 3)).toEqual('3 വ്യക്തികൾ');
    expect(Translator.t('{n} വ്യക്തി|{n} വ്യക്തികൾ', 1.2)).toEqual('1.2 വ്യക്തികൾ');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>നിഒദ്നൊഗൊ വ്യക്തി|1<###>വ്യക്തി|n>1<###>{n} ആളുകൾ', 0)).toEqual('നിഒദ്നൊഗൊ വ്യക്തി');
    expect(Translator.t('0<###>നിഒദ്നൊഗൊ വ്യക്തി|1<###>വ്യക്തി|n>1<###>{n} ആളുകൾ', 1)).toEqual('വ്യക്തി');
    expect(Translator.t('0<###>നിഒദ്നൊഗൊ വ്യക്തി|1<###>വ്യക്തി|n>1<###>{n} ആളുകൾ', 2)).toEqual('2 ആളുകൾ');
    expect(Translator.t('0<###>നിഒദ്നൊഗൊ വ്യക്തി|1<###>വ്യക്തി|n>1<###>{n} ആളുകൾ', 1.2)).toEqual('1.2 ആളുകൾ');
  });
});

describe('translate gujarati', () => {
  beforeEach(() => {
    Language.set(Language.gujarati());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('કિલોગ્રામ')).toEqual('કિલોગ્રામ');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} કિલોગ્રામ|{n} કિલોગ્રામ્સ', 0)).toEqual('0 કિલોગ્રામ');
    expect(Translator.t('{n} કિલોગ્રામ|{n} કિલોગ્રામ્સ', 1)).toEqual('1 કિલોગ્રામ');
    expect(Translator.t('{n} કિલોગ્રામ|{n} કિલોગ્રામ્સ', 2)).toEqual('2 કિલોગ્રામ્સ');
    expect(Translator.t('{n} કિલોગ્રામ|{n} કિલોગ્રામ્સ', 3)).toEqual('3 કિલોગ્રામ્સ');
    expect(Translator.t('{n} કિલોગ્રામ|{n} કિલોગ્રામ્સ', 1.2)).toEqual('1.2 કિલોગ્રામ્સ');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>{n} કિલોગ્રામ|1<###>{n} કિલોગ્રામ|n>1<###>{n} કિલોગ્રામ', 0)).toEqual('0 કિલોગ્રામ');
    expect(Translator.t('0<###>{n} કિલોગ્રામ|1<###>{n} કિલોગ્રામ|n>1<###>{n} કિલોગ્રામ', 1)).toEqual('1 કિલોગ્રામ');
    expect(Translator.t('0<###>{n} કિલોગ્રામ|1<###>{n} કિલોગ્રામ|n>1<###>{n} કિલોગ્રામ', 2)).toEqual('2 કિલોગ્રામ');
    expect(Translator.t('0<###>{n} કિલોગ્રામ|1<###>{n} કિલોગ્રામ|n>1<###>{n} કિલોગ્રામ', 1.2)).toEqual('1.2 કિલોગ્રામ');
  });
});

describe('translate kannada', () => {
  beforeEach(() => {
    Language.set(Language.kannada());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('ದಿನ')).toEqual('ದಿನ');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 0)).toEqual('0 ದಿನ');
    expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 1)).toEqual('1 ದಿನ');
    expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 2)).toEqual('2 ದಿನಗಳು');
    expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 3)).toEqual('3 ದಿನಗಳು');
    expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 1.2)).toEqual('1.2 ದಿನಗಳು');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>{n} ದಿನ|1<###>{n} ದಿನ|n>1<###>{n} ದಿನಗಳು', 0)).toEqual('0 ದಿನ');
    expect(Translator.t('0<###>{n} ದಿನ|1<###>{n} ದಿನ|n>1<###>{n} ದಿನಗಳು', 1)).toEqual('1 ದಿನ');
    expect(Translator.t('0<###>{n} ದಿನ|1<###>{n} ದಿನ|n>1<###>{n} ದಿನಗಳು', 2)).toEqual('2 ದಿನಗಳು');
    expect(Translator.t('0<###>{n} ದಿನ|1<###>{n} ದಿನ|n>1<###>{n} ದಿನಗಳು', 1.2)).toEqual('1.2 ದಿನಗಳು');
  });
});

describe('translate bengali', () => {
  beforeEach(() => {
    Language.set(Language.bengali());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('সম্প্রদায়')).toEqual('সম্প্রদায়');
  });

  // // У них походу не пишутся числа

  // it('should translate plural form with placeholders correctly', function () {
  //  expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 0)).toEqual('0 ದಿನ');
  //  expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 1)).toEqual('1 ದಿನ');
  //  expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 2)).toEqual('2 ದಿನಗಳು');
  //  expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 3)).toEqual('3 ದಿನಗಳು');
  //  expect(Translator.t('{n} ದಿನ|{n} ದಿನಗಳು', 1.2)).toEqual('1.2 ದಿನಗಳು');
  // });

  // it('should translate plural form with expressions correctly', function () {
  //  expect(Translator.t('0<###>{n} ದಿನ|1<###>{n} ದಿನ|n>1<###>{n} ದಿನಗಳು', 0)).toEqual('0 ದಿನ');
  //  expect(Translator.t('0<###>{n} ದಿನ|1<###>{n} ದಿನ|n>1<###>{n} ದಿನಗಳು', 1)).toEqual('1 ದಿನ');
  //  expect(Translator.t('0<###>{n} ದಿನ|1<###>{n} ದಿನ|n>1<###>{n} ದಿನಗಳು', 2)).toEqual('2 ದಿನಗಳು');
  //  expect(Translator.t('0<###>{n} ದಿನ|1<###>{n} ದಿನ|n>1<###>{n} ದಿನಗಳು', 1.2))
  //    .toEqual('1.2 ದಿನಗಳು');
  // });
});

describe('translate telugu', () => {
  beforeEach(() => {
    Language.set(Language.telugu());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('ప్రజలు')).toEqual('ప్రజలు');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} ప్రజలు|{n} మంది', 0)).toEqual('0 మంది');
    expect(Translator.t('{n} ప్రజలు|{n} మంది', 1)).toEqual('1 ప్రజలు');
    expect(Translator.t('{n} ప్రజలు|{n} మంది', 2)).toEqual('2 మంది');
    expect(Translator.t('{n} ప్రజలు|{n} మంది', 3)).toEqual('3 మంది');
    expect(Translator.t('{n} ప్రజలు|{n} మంది', 1.2)).toEqual('1.2 మంది');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>{n} మంది|1<###>{n} ప్రజలు|n>1<###>{n} మంది', 0)).toEqual('0 మంది');
    expect(Translator.t('0<###>{n} మంది|1<###>{n} ప్రజలు|n>1<###>{n} మంది', 1)).toEqual('1 ప్రజలు');
    expect(Translator.t('0<###>{n} మంది|1<###>{n} ప్రజలు|n>1<###>{n} మంది', 2)).toEqual('2 మంది');
    expect(Translator.t('0<###>{n} మంది|1<###>{n} ప్రజలు|n>1<###>{n} మంది', 1.2)).toEqual('1.2 మంది');
  });
});

describe('translate marathi', () => {
  beforeEach(() => {
    Language.set(Language.marathi());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('लोक')).toEqual('लोक');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} लोक|{n} लोक', 0)).toEqual('0 लोक');
    expect(Translator.t('{n} लोक|{n} लोक', 1)).toEqual('1 लोक');
    expect(Translator.t('{n} लोक|{n} लोक', 2)).toEqual('2 लोक');
    expect(Translator.t('{n} लोक|{n} लोक', 3)).toEqual('3 लोक');
    expect(Translator.t('{n} लोक|{n} लोक', 1.2)).toEqual('1.2 लोक');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>एकच व्यक्ती|1<###>{n} लोक|n>1<###>{n} लोक', 0)).toEqual('एकच व्यक्ती');
    expect(Translator.t('0<###>एकच व्यक्ती|1<###>{n} लोक|n>1<###>{n} लोक', 1)).toEqual('1 लोक');
    expect(Translator.t('0<###>एकच व्यक्ती|1<###>{n} लोक|n>1<###>{n} लोक', 2)).toEqual('2 लोक');
    expect(Translator.t('0<###>एकच व्यक्ती|1<###>{n} लोक|n>1<###>{n} लोक', 1.2)).toEqual('1.2 लोक');
  });
});

describe('translate tamil', () => {
  beforeEach(() => {
    Language.set(Language.tamil());
  });

  it('should translate simple form correctly', () => {
    expect(Translator.t('மக்கள்')).toEqual('மக்கள்');
  });

  it('should translate plural form with placeholders correctly', () => {
    expect(Translator.t('{n} ஆப்பிள்|{n} ஆப்பிள்கள்', 0)).toEqual('0 ஆப்பிள்கள்');
    expect(Translator.t('{n} ஆப்பிள்|{n} ஆப்பிள்கள்', 1)).toEqual('1 ஆப்பிள்');
    expect(Translator.t('{n} ஆப்பிள்|{n} ஆப்பிள்கள்', 2)).toEqual('2 ஆப்பிள்கள்');
    expect(Translator.t('{n} ஆப்பிள்|{n} ஆப்பிள்கள்', 3)).toEqual('3 ஆப்பிள்கள்');
    expect(Translator.t('{n} ஆப்பிள்|{n} ஆப்பிள்கள்', 1.2)).toEqual('1.2 ஆப்பிள்கள்');
  });

  it('should translate plural form with expressions correctly', () => {
    expect(Translator.t('0<###>{n} ஆப்பிள்கள்|1<###>{n} ஆப்பிள்|n>1<###>{n} ஆப்பிள்கள்', 0)).toEqual('0 ஆப்பிள்கள்');
    expect(Translator.t('0<###>{n} ஆப்பிள்கள்|1<###>{n} ஆப்பிள்|n>1<###>{n} ஆப்பிள்கள்', 1)).toEqual('1 ஆப்பிள்');
    expect(Translator.t('0<###>{n} ஆப்பிள்கள்|1<###>{n} ஆப்பிள்|n>1<###>{n} ஆப்பிள்கள்', 2)).toEqual('2 ஆப்பிள்கள்');
    expect(Translator.t('0<###>{n} ஆப்பிள்கள்|1<###>{n} ஆப்பிள்|n>1<###>{n} ஆப்பிள்கள்', 1.2)).toEqual('1.2 ஆப்பிள்கள்');
  });
});

describe('Using React Component as param in t() function', () => {
  beforeEach(() => {
    Language.set(Language.english());
  });

  it('should return correct string with component markup', () => {
    const val = 100;
    const Component = ({ value }) => (
      <div className="test"><span>{ value }</span></div>
    );

    Component.propTypes = {
      value: PropTypes.number.isRequired,
    };

    expect(Translator.t('{summ} on hold', {
      summ: <Component value={val} />,
    })).toEqual(`<div class="test"><span>${val}</span></div> on hold`);

    expect(Translator.t('0<###>{component} has no mail|n>0<###>{component} has {n} mails', {
      n: 0,
      component: <Component value={val} />,
    })).toEqual(`<div class="test"><span>${val}</span></div> has no mail`);

    expect(Translator.t('0<###>{component} has no mail|n>0<###>{component} has {n} mails', {
      n: 4,
      component: <Component value={val} />,
    })).toEqual(`<div class="test"><span>${val}</span></div> has 4 mails`);
  });
});

