import Plural from '../plural';
import Language from '../language';

describe('plural forms for russian', () => {
  let lang;

  beforeEach(() => {
    lang = Language.russian();
  });

  it('should choose plural form correctly', () => {
    // one
    expect(Plural.form(lang, 1)).toEqual(Plural.FORMS.RU.ONE);

    // few
    expect(Plural.form(lang, 2)).toEqual(Plural.FORMS.RU.FEW);
    expect(Plural.form(lang, 1.2)).toEqual(Plural.FORMS.RU.FEW);

    // many
    expect(Plural.form(lang, 0)).toEqual(Plural.FORMS.RU.MANY);
    expect(Plural.form(lang, 5)).toEqual(Plural.FORMS.RU.MANY);
    expect(Plural.form(lang, 11)).toEqual(Plural.FORMS.RU.MANY);
    expect(Plural.form(lang, 12)).toEqual(Plural.FORMS.RU.MANY);
    expect(Plural.form(lang, 13)).toEqual(Plural.FORMS.RU.MANY);
    expect(Plural.form(lang, 14)).toEqual(Plural.FORMS.RU.MANY);
  });
});

describe('plural forms for english', () => {
  let lang;

  beforeEach(() => {
    lang = Language.english();
  });

  it('should choose plural form correctly', () => {
    // one
    expect(Plural.form(lang, 1)).toEqual(Plural.FORMS.EN.ONE);

    // other
    expect(Plural.form(lang, 0)).toEqual(Plural.FORMS.EN.OTHER);
    expect(Plural.form(lang, 2)).toEqual(Plural.FORMS.EN.OTHER);
    expect(Plural.form(lang, 5)).toEqual(Plural.FORMS.EN.OTHER);
    expect(Plural.form(lang, 1.2)).toEqual(Plural.FORMS.EN.OTHER);
  });
});

describe('plural forms for hindi', () => {
  let lang;

  beforeEach(() => {
    lang = Language.hindi();
  });

  it('should choose plural form correctly', () => {
    // one
    expect(Plural.form(lang, 0)).toEqual(Plural.FORMS.HI.ONE);
    expect(Plural.form(lang, 1)).toEqual(Plural.FORMS.HI.ONE);

    // other
    expect(Plural.form(lang, 2)).toEqual(Plural.FORMS.HI.OTHER);
    expect(Plural.form(lang, 5)).toEqual(Plural.FORMS.HI.OTHER);
    expect(Plural.form(lang, 1.2)).toEqual(Plural.FORMS.HI.OTHER);
  });
});

describe('plural forms for unkrainian', () => {
  let lang;

  beforeEach(() => {
    lang = Language.ukrainian();
  });

  it('should choose plural form correctly', () => {
    // one
    expect(Plural.form(lang, 1)).toEqual(Plural.FORMS.UK.ONE);

    // few
    expect(Plural.form(lang, 2)).toEqual(Plural.FORMS.UK.FEW);

    // many
    expect(Plural.form(lang, 0)).toEqual(Plural.FORMS.UK.MANY);
    expect(Plural.form(lang, 5)).toEqual(Plural.FORMS.UK.MANY);
    expect(Plural.form(lang, 11)).toEqual(Plural.FORMS.UK.MANY);
    expect(Plural.form(lang, 12)).toEqual(Plural.FORMS.UK.MANY);
    expect(Plural.form(lang, 13)).toEqual(Plural.FORMS.UK.MANY);
    expect(Plural.form(lang, 14)).toEqual(Plural.FORMS.UK.MANY);

    // other
    expect(Plural.form(lang, 1.2)).toEqual(Plural.FORMS.UK.OTHER);
  });
});

describe('plural forms for german', () => {
  let lang;

  beforeEach(() => {
    lang = Language.german();
  });

  it('should choose plural form correctly', () => {
    // one
    expect(Plural.form(lang, 1)).toEqual(Plural.FORMS.DE.ONE);

    // other
    expect(Plural.form(lang, 0)).toEqual(Plural.FORMS.DE.OTHER);
    expect(Plural.form(lang, 2)).toEqual(Plural.FORMS.DE.OTHER);
    expect(Plural.form(lang, 5)).toEqual(Plural.FORMS.DE.OTHER);
    expect(Plural.form(lang, 999)).toEqual(Plural.FORMS.DE.OTHER);
    expect(Plural.form(lang, 1.2)).toEqual(Plural.FORMS.DE.OTHER);
  });
});

describe('plural forms for polish', () => {
  let lang;

  beforeEach(() => {
    lang = Language.polish();
  });

  it('should choose plural form correctly', () => {
    // one
    expect(Plural.form(lang, 1)).toEqual(Plural.FORMS.PL.ONE);

    // few
    expect(Plural.form(lang, 2)).toEqual(Plural.FORMS.PL.FEW);
    expect(Plural.form(lang, 3)).toEqual(Plural.FORMS.PL.FEW);
    expect(Plural.form(lang, 4)).toEqual(Plural.FORMS.PL.FEW);
    expect(Plural.form(lang, 22)).toEqual(Plural.FORMS.PL.FEW);
    expect(Plural.form(lang, 32)).toEqual(Plural.FORMS.PL.FEW);

    // many
    expect(Plural.form(lang, 0)).toEqual(Plural.FORMS.PL.MANY);
    expect(Plural.form(lang, 5)).toEqual(Plural.FORMS.PL.MANY);
    expect(Plural.form(lang, 11)).toEqual(Plural.FORMS.PL.MANY);
    expect(Plural.form(lang, 12)).toEqual(Plural.FORMS.PL.MANY);
    expect(Plural.form(lang, 13)).toEqual(Plural.FORMS.PL.MANY);
    expect(Plural.form(lang, 14)).toEqual(Plural.FORMS.PL.MANY);
    expect(Plural.form(lang, 21)).toEqual(Plural.FORMS.PL.MANY);
    expect(Plural.form(lang, 35)).toEqual(Plural.FORMS.PL.MANY);

    // other
    expect(Plural.form(lang, 1.2)).toEqual(Plural.FORMS.PL.OTHER);
  });
});
