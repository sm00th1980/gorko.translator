'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* eslint "no-eval": "off" */

var React = require('react');
var ReactDOMServer = require('react-dom/server');

var Plural = require('./plural');
var Language = require('./language');

var isUndefined = function isUndefined(value) {
  return typeof value === 'undefined';
};

var Gorko = {
  translate: {
    dictionary: {},
    config: { language: '' }
  }
};

/**
 * Process translations
 *
 * @param message the message to be translated
 * @param params array of parameters (number, placeholders)
 * @param dictionary instance of dictionary
 * @return translated string
 */
Gorko.translate.process = function process(message, params_, dictionary) {
  var params = params_;
  // try to translate string
  var translation = dictionary && !isUndefined(dictionary[message]) ? dictionary[message] : message;

  if (isUndefined(params)) {
    params = 0;
  }

  // declare numeric param
  var num = 0;

  // extract number from params
  if (Number(params) || params === 0) {
    params = { n: params }; // param is numeric, convert to object key for convenience
  }

  if (Number(params.n) || params.n === 0) {
    num = params.n;
  }

  Object.keys(params).forEach(function (key) {
    if (React.isValidElement(params[key])) {
      params[key] = ReactDOMServer.renderToStaticMarkup(params[key]);
    }
  });

  // split translation into pieces
  var chunks = translation.split('|');

  if (translation.indexOf('<###>') !== -1) {
    // translation contains expression
    for (var i = 0; i < chunks.length; i += 1) {
      var pieces = chunks[i].split('<###>'); // split each chunk in two parts (0: expression, 1: message)
      var ex = pieces[0];
      var msg = pieces[1];

      if (pieces.length === 2) {
        // handle number shortcut (0 instead of n==0)
        if (ex % 1 === 0) {
          ex = 'n==' + ex;
        }

        // create expression to be evaluated (e.g. n>3)
        var evalExpr = ex.split('n').join(num);

        // if expression matches, set translation to current chunk
        if (eval(evalExpr)) {
          translation = msg;
          break;
        }
      }
    }
  } else if (chunks.length > 1) {
    // if translation doesn't contain # but does contain |, treat it as simple choice format

    var index = Plural.form(Language.currentLanguage(), num);
    translation = chunks[index];
  }

  // replace placeholder/replacements
  if ((typeof params === 'undefined' ? 'undefined' : _typeof(params)) === 'object') {
    Object.keys(params).forEach(function (key) {
      translation = translation.split('{' + key + '}').join(params[key]);
    });
  }

  return translation;
};

/**
 * Wrapper for port Gorko::t
 *
 * @param message the message to be translated
 * @param params array of parameters (number, placeholders)
 * @return translated message
 */

var Translator = function () {
  function Translator() {
    _classCallCheck(this, Translator);
  }

  _createClass(Translator, null, [{
    key: 't',
    value: function t(text, data) {
      return Gorko.translate.process(text, data, false);
    }
  }]);

  return Translator;
}();

module.exports = Translator;