'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var currentLanguageValue = null;

var isString = function isString(value) {
  return typeof value === 'string';
};
var includes = function includes(array, element) {
  return array.indexOf(element) !== -1;
};

var Language = function () {
  function Language() {
    _classCallCheck(this, Language);
  }

  _createClass(Language, null, [{
    key: 'currentLanguage',
    value: function currentLanguage() {
      var _currentLanguage = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : currentLanguageValue;

      if (this.isValid(_currentLanguage)) {
        return _currentLanguage;
      }
      currentLanguageValue = Language.defaultLanguage();

      return currentLanguageValue;
    }
  }, {
    key: 'defaultLanguage',
    value: function defaultLanguage() {
      if (this.isValid(global.LANG)) {
        return global.LANG.toLowerCase();
      }
      return this.english();
    }
  }, {
    key: 'supportedLanguages',
    value: function supportedLanguages() {
      return [this.russian(), this.english(), this.hindi(), this.polish(), this.german(), this.ukrainian(), this.thai(), this.punjabi(), this.malayalam(), this.gujarati(), this.kannada(), this.bengali(), this.telugu(), this.marathi(), this.tamil()];
    }
  }, {
    key: 'set',
    value: function set(language) {
      if (this.isValid(language)) {
        currentLanguageValue = language.toLowerCase();
      }
    }
  }, {
    key: 'russian',
    value: function russian() {
      return 'ru';
    }
  }, {
    key: 'isCurrent',
    value: function isCurrent(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.currentLanguage());
    }
  }, {
    key: 'isRussian',
    value: function isRussian(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.russian());
    }
  }, {
    key: 'english',
    value: function english() {
      return 'en';
    }
  }, {
    key: 'isEnglish',
    value: function isEnglish(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.english());
    }
  }, {
    key: 'ukrainian',
    value: function ukrainian() {
      return 'uk';
    }
  }, {
    key: 'isUkrainian',
    value: function isUkrainian(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.ukrainian());
    }
  }, {
    key: 'hindi',
    value: function hindi() {
      return 'hi';
    }
  }, {
    key: 'isHindi',
    value: function isHindi(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.hindi());
    }
  }, {
    key: 'german',
    value: function german() {
      return 'de';
    }
  }, {
    key: 'isGerman',
    value: function isGerman(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.german());
    }
  }, {
    key: 'polish',
    value: function polish() {
      return 'pl';
    }
  }, {
    key: 'isPolish',
    value: function isPolish(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.polish());
    }
  }, {
    key: 'thai',
    value: function thai() {
      return 'th';
    }
  }, {
    key: 'isThai',
    value: function isThai(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.thai());
    }
  }, {
    key: 'punjabi',
    value: function punjabi() {
      return 'pa';
    }
  }, {
    key: 'isPunjabi',
    value: function isPunjabi(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.punjabi());
    }
  }, {
    key: 'malayalam',
    value: function malayalam() {
      return 'ml';
    }
  }, {
    key: 'isMalayalam',
    value: function isMalayalam(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.malayalam());
    }
  }, {
    key: 'gujarati',
    value: function gujarati() {
      return 'gu';
    }
  }, {
    key: 'isGujarati',
    value: function isGujarati(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.gujarati());
    }
  }, {
    key: 'kannada',
    value: function kannada() {
      return 'kn';
    }
  }, {
    key: 'isKannada',
    value: function isKannada(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.kannada());
    }
  }, {
    key: 'bengali',
    value: function bengali() {
      return 'bn';
    }
  }, {
    key: 'isBengali',
    value: function isBengali(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.bengali());
    }
  }, {
    key: 'telugu',
    value: function telugu() {
      return 'te';
    }
  }, {
    key: 'isTelugu',
    value: function isTelugu(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.telugu());
    }
  }, {
    key: 'marathi',
    value: function marathi() {
      return 'mr';
    }
  }, {
    key: 'isMarathi',
    value: function isMarathi(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.marathi());
    }
  }, {
    key: 'tamil',
    value: function tamil() {
      return 'ta';
    }
  }, {
    key: 'isTamil',
    value: function isTamil(language) {
      return !!(this.isValid(language) && language.toLowerCase() === this.tamil());
    }
  }, {
    key: 'isValid',
    value: function isValid(language) {
      return !!(language && isString(language) && includes(Language.supportedLanguages(), language.toLowerCase()));
    }
  }]);

  return Language;
}();

module.exports = Language;